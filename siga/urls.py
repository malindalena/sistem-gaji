from django.contrib import admin
from django.urls import path

from sigas import views as sigas

urlpatterns = [
path('admin/', admin.site.urls),

path('', sigas.index, name='login'),

path('siga/', sigas.dashboard, name='dashboard'),

path('siga/data/bulan', sigas.viewbulan, name='bulan'),
path('siga/data/bulan/tambah',sigas.addbulan, name='bulan_add'),
path('siga/data/bulan/edit/<int:pk>', sigas.editbulan,name='bulan_edit'),
path('siga/data/bulan/delete/<int:pk>', sigas.deletebulan,name='bulan_delete'),

path('siga/data/bank', sigas.viewbank, name='bank'),
path('siga/data/bank/tambah',sigas.addbank, name='bank_add'),
path('siga/data/bank/edit/<int:pk>', sigas.editbank,name='bank_edit'),
path('siga/data/bank/delete/<int:pk>', sigas.deletebank,name='bank_delete'),


# ---JABATAN---
path('siga/data/jabatan', sigas.viewjabatan, name='jabatan'),
path('siga/data/jabatan/tambah',sigas.addjabatan, name='jabatan_add'),
path('siga/data/jabatan/edit/<int:pk>', sigas.editjabatan,name='jabatan_edit'),
path('siga/data/jabatan/delete/<int:pk>', sigas.deletejabatan,name='jabatan_delete'),

# ---GAJI POKOK---
path('siga/gaji/gajipokok', sigas.viewgajipokok, name='gajipokok'),
path('siga/gaji/gajipokok/tambah',sigas.addgajipokok, name='gajipokok_add'),
path('siga/gaji/gajipokok/edit/<int:pk>', sigas.editgajipokok,name='gajipokok_edit'),
path('siga/gaji/gajipokok/delete/<int:pk>', sigas.deletegajipokok,name='gajipokok_delete'),


# ---GAJI BULANAN---
path('siga/gaji/bulanan', sigas.viewbulanan, name='bulanan'),
path('siga/gaji/bulanan/tambah',sigas.addbulanan, name='bulanan_add'),
path('siga/gaji/bulanan/edit/<int:pk>', sigas.editbulanan,name='bulanan_edit'),
path('siga/gaji/bulanan/delete/<int:pk>', sigas.deletebulanan,name='bulanan_delete'),
path('siga/gaji/bulanan/print/<int:pk>', sigas.printbulanan,name='bulanan_print'),

# ---PINJAMAN---
path('siga/gaji/pinjaman', sigas.viewpinjaman, name='pinjaman'),
path('siga/gaji/pinjaman/tambah',sigas.addpinjaman, name='pinjaman_add'),
path('siga/gaji/pinjaman/edit/<int:pk>', sigas.editpinjaman,name='pinjaman_edit'),
path('siga/gaji/pinjaman/delete/<int:pk>', sigas.deletepinjaman,name='pinjaman_delete'),

# ---PEGAWAI---
path('siga/pegawai/pegawai_aktif', sigas.viewpegawai, name='pegawai'),
path('siga/pegawai/pegawai_aktif/tambah',sigas.addpegawai, name='pegawai_add'),
path('siga/pegawai/pegawai_aktif/edit', sigas.editpegawai,name='pegawai_edit'),
path('siga/pegawai/pegawai_aktif/delete/<int:pk>', sigas.deletepegawai,name='pegawai_delete'),
path('siga/pegawai/pegawai_aktif/show/<int:pk>', sigas.showpegawai,name='pegawai_show'),
]
