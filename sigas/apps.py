from django.apps import AppConfig


class SigasConfig(AppConfig):
    name = 'sigas'
