from django.db import models

# Create your models here.
class bulan(models.Model):
	id_bulan = models.AutoField(primary_key=True)
	nama_bulan = models.CharField(max_length=100)
	keterangan = models.CharField(max_length=100)
	class Meta:
		db_table = "tb_bulan"

class jabatan(models.Model):
    id_jabatan = models.AutoField(primary_key=True)
    nama_jabatan = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=100)
    class Meta:
        db_table = "tb_jabatan"

class bank(models.Model):
	id_bank = models.AutoField(primary_key=True)
	nama_bank = models.CharField(max_length=100)
	keterangan = models.CharField(max_length=100)
	class Meta:
		db_table = "tb_bank"


class gajipokok(models.Model):
	id_gajipokok = models.AutoField(primary_key=True)
	tgl_bayar = models.DateField(null=True,blank=True)
	nama_pegawai = models.CharField(max_length=100)
	jabatan = models.CharField(max_length=100)
	tmt = models.DateField(blank=True, null=True)
	gaji_pokok = models.DecimalField(max_digits=20, decimal_places=3)
	class Meta:
		db_table = "tb_gajipokok"


class bulanan(models.Model):
	id_bulanan = models.AutoField(primary_key=True)
	nama_pegawai = models.CharField(max_length=100)
	jabatan = models.CharField(max_length=100)
	gaji_pokok = models.CharField(max_length=100)
	bonus = models.CharField(max_length=100)
	bpjs = models.CharField(max_length=100)
	pinjaman = models.CharField(max_length=100)
	total_gaji = models.CharField(max_length=100)
	periode_pembayaran = models.CharField(max_length=100)
	class Meta:
		db_table = "tb_bulanan"

class pinjaman(models.Model):
    id_pinjaman = models.AutoField(primary_key=True)
    nama_pegawai = models.CharField(max_length=100)
    tanggal_pinjam = models.DateField()
    jumlah_pinjam = models.CharField(max_length=100)
    jenis_transaksi = models.CharField(max_length=100)
    keterangan = models.CharField(max_length=100)
    class Meta:
        db_table = "tb_pinjaman"


class pegawai(models.Model):
    id_pegawai = models.AutoField(primary_key=True)
    no_pegawai = models.CharField(max_length=100)
    nama_pegawai = models.CharField(max_length=100)
    no_ktp = models.CharField(max_length=100)
    tempat_lahir = models.CharField(max_length=100)
    tgl_lahir = models.DateField(auto_now=False, auto_now_add=False)
    alamat = models.CharField(max_length=100)
    npwp = models.BooleanField()
    email = models.EmailField(max_length=70, null=True, blank=True, unique=True)
    no_hp = models.CharField(max_length=13)
    foto = models.FileField(upload_to='documents/')
    bank = models.CharField(max_length=50)
    nama = models.CharField(max_length=100)
    no_rekening = models.DecimalField(max_digits=50, decimal_places=0)
    kantor_cabang = models.CharField(max_length=100)
    no_surat_keterangan = models.CharField(max_length=100)
    jabatan = models. CharField(max_length=100)
    jenis_perjanjian = models. CharField(max_length=100)
    mulai_perjanjian_kerja = models.DateField(auto_now=False, auto_now_add=False)
    akhir_perjanjian_kerja = models.DateField(auto_now=False, auto_now_add=False)
    upah = models.DecimalField(max_digits=20, decimal_places=3)
    keterangan = models.CharField(max_length=100)
    class Meta:
        db_table = "tb_pegawai"