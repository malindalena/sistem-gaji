from django.forms import Textarea, ModelForm, Select
from django import forms
from sigas.models import bulan, jabatan, bank, gajipokok, bulanan, pinjaman, pegawai
from bootstrap_datepicker_plus import DatePickerInput
from crispy_forms.bootstrap import Tab, TabHolder, FormActions
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout, Button

# -------------+
# FORM MERK    |
# -------------+
class Bulanform(ModelForm):
    nama_bulan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Bulan'
                }
            ),
            required=True
        )

    keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Keterangan'
                }
            ),
            required=True
        )
    class Meta:
        model = bulan
        fields = ['nama_bulan', 'keterangan']

# ----------------+
# FORM JABATAN    |
# ----------------+

class Jabatanform(ModelForm):
    nama_jabatan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Jabatan'
                }
            ),
            required=True
        )

    keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Keterangan'
                }
            ),
            required=True
        )
    class Meta:
        model = jabatan
        fields = ['nama_jabatan', 'keterangan']


# -------------+
# FORM BANK    |
# -------------+
class Bankform(ModelForm):
    nama_bank = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Bank'
                }
            ),
            required=True
        )

    keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Keterangan'
                }
            ),
            required=True
        )
    class Meta:
        model = bank
        fields = ['nama_bank', 'keterangan']


# ------------------+
# FORM GAJI POKOK   |
# ------------------+

class Gajipokokform(ModelForm):
    tgl_bayar = forms.DateField(
        widget=DatePickerInput(format='%Y-%m-%d')
        )

    nama_pegawai = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Pegawai'
                }
            )
        )

    jabatan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Jabatan'
                }
            )
        )

    tmt = forms.DateField(
        widget=DatePickerInput(format='%Y-%m-%d')
        )

    gaji_pokok = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Jumlah Gaji Pokok'
            }
            )
        )

    class Meta:
        model = gajipokok
        fields = ['tgl_bayar', 'nama_pegawai', 'jabatan', 'tmt', 'gaji_pokok']



# ---------------------+
# FORM GAJI BULANAN    |
# ---------------------+
class Bulananform(ModelForm):
    nama_pegawai = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama pegawai'
                }
            ),
            required=True
        )

    jabatan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan jabatan'
                }
            ),
            required=True
        )
    gaji_pokok = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan gaji pokok'
                }
            ),
            required=True
        )
    bonus = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan bonus'
                }
            ),
            required=False
        )
    bpjs = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan bpjs'
                }
            ),
            required=False
        )
    pinjaman = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan pinjaman'
                }
            ),
            required=False
        )
    total_gaji = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan total gaji'
                }
            ),
            required=True
        )
    periode_pembayaran = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan bulan'
                }
            ),
            required=True
        )
    class Meta:
        model = bulanan
        fields = ['nama_pegawai', 'jabatan', 'gaji_pokok', 'bonus', 'bpjs', 'pinjaman', 'total_gaji', 'periode_pembayaran']


# ----------------+
# FORM PINJAMAN  |
# ----------------+

class Pinjamanform(ModelForm):
    nama_pegawai = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Pegawai'
                }
            ),
            required=True
        )

    tanggal_pinjam = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Pilih Tanggal Pinjam/Bayar',
                'data-date-format':"yyyy/mm/dd",
                'id':"date"

                }
            ),
            required=True
        )
    jumlah_pinjam = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Jumlah Pinjaman'
                }
            ),
            required=True
        )
    jenis_transaksi = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan keterangan'
                }
            ),
            required=True
        )
        
        
    keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan keterangan'
                }
            ),
            required=True
        )
    class Meta:
        model = pinjaman
        fields = ['nama_pegawai', 'tanggal_pinjam', 'jumlah_pinjam', 'jenis_transaksi', 'keterangan']

class Pegawaiform(ModelForm):
    no_pegawai = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan No Pegawai'
                }
            )
        )

    nama_pegawai = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Nama Pegawai'
                }
            )
        )

    no_ktp = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan No KTP'
                }
            )
        )

    tempat_lahir = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Tempat Lahir'
                }
            )
        )

    tgl_lahir = forms.DateField(
        widget=DatePickerInput(format='%m/%d/%Y')
        )

    alamat = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Alamat'
                }
            )
        )

    npwp = forms.BooleanField(
        widget=forms.CheckboxInput(
            attrs={
                'class':'checkbox',
                }
            )
        )

    email = forms.EmailField(
        widget=forms.EmailInput(
            attrs={
            'class': 'contatct-form'
                }
            )
        )

    no_hp = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan No Telp/Hp'
                }
            )
        )

    foto = forms.FileField(
        )

    bank = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'name': 'bank[]',
                'class':'form-control',
                'placeholder':'Isikan Nama Bank'
                }
            )
        )

    nama = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'name': 'nama[]',
                'class':'form-control',
                'placeholder':'Isikan Nama'
                }
            )
        )

    no_rekening = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'name': 'no_rekening[]',
                'class':'form-control',
                'placeholder':'Isikan No. Rekening'
            }
            )
        )

    kantor_cabang = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'name':'kantor_cabang[]',
                'class':'form-control',
                'placeholder':'Isikan Nama Kantor Cabang'
                }
            )
        )

    no_surat_keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan No. Surat Keterangan'
                }
            )
        )

    jabatan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Jabatan'
                }
            )
        )

    jenis_perjanjian = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Jenis Perjanjian'
                }
            )
        )

    mulai_perjanjian_kerja = forms.DateField(
        widget=DatePickerInput(format='%m/%d/%Y')
        )

    akhir_perjanjian_kerja = forms.DateField(
        widget=DatePickerInput(format='%m/%d/%Y')
        )

    upah = forms.DecimalField(
        widget=forms.NumberInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Jumlah Upah'
            }
            )
        )

    keterangan = forms.CharField(
        widget=forms.TextInput(
            attrs={
                'class':'form-control',
                'placeholder':'Isikan Keterangan'
                }
            )
        )

    # Pegawaiform = formset_factory(Pegawaiform, extra=1)

    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_tag = False
        self.helper.layout = Layout(
            TabHolder(
                Tab('Biodata',
                    'no_pegawai',
                    'nama_pegawai',
                    'no_ktp',
                    'tempat_lahir',
                    'tgl_lahir',
                    'alamat',
                    'npwp',
                    'email',
                    'no_hp',
                    'foto'
                    ),
                Tab('Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    'Rekening Bank',
                    'bank',
                    'nama',
                    'no_rekening',
                    'kantor_cabang',
                    # FormActions(
                    #     Button('addMore', 'Tambah Bank Rekening')
                    #     )
                ),
                Tab('Perjanjian Kerja',
                    'no_surat_keterangan',
                    'jabatan',
                    'jenis_perjanjian',
                    'mulai_perjanjian_kerja',
                    'akhir_perjanjian_kerja',
                    'upah',
                    'keterangan'
                )
            )
        )
        FormActions(
            Submit('submit', 'Simpan'),
            Submit('submit', 'Simpan dan Tambah'),
            Button('cancel', 'Kembali')
        )

    class Meta:
        model = pegawai
        fields = ['no_pegawai', 'nama_pegawai', 'no_ktp', 'tempat_lahir', 'tgl_lahir', 'alamat', 'npwp', 'email',
                  'no_hp', 'foto', 'bank', 'nama', 'no_rekening', 'kantor_cabang', 'no_surat_keterangan', 'jabatan',
                  'jenis_perjanjian', 'mulai_perjanjian_kerja', 'akhir_perjanjian_kerja', 'upah', 'keterangan']

