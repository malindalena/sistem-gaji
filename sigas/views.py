from django.shortcuts import render, redirect
from sigas.models import bulan, bank, jabatan, gajipokok, bulanan, pinjaman, pegawai
from sigas.forms import Bulanform, Bankform, Jabatanform, Gajipokokform, Bulananform, Pinjamanform, Pegawaiform
from django.core.paginator import Paginator


# Create your views here.

def index(request):
    return render(request, 'login.html')

def dashboard(request):
    return render(request, 'dashboard.html')

def viewbulan(request):
    daftar_bulan = bulan.objects.all()
    pagination = Paginator(daftar_bulan,5)

    page = request.GET.get('page','')
    bulan_pg = pagination.get_page(page)
    return render(request, 'data/bulan/bulan.html', {'daftar_bulan': bulan_pg})

def addbulan(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Bulanform(form_data)
        if form.is_valid():
            Bulan = bulan(
                nama_bulan=request.POST['nama_bulan'],
                keterangan=request.POST['keterangan']
             )
            Bulan.save()
            return redirect('/siga/data/bulan')
    else:
        form = Bulanform()
    return render(request, 'data/bulan/bulan_tambah.html', {'form': form})

def editbulan(request,pk):
    Bulan = bulan.objects.get(pk=pk)
    if request.method == "POST":
        form = Bulanform(request.POST, instance=Bulan)
        if form.is_valid():
            Bulan = form.save(commit=False)
            nama_bulan = request.POST['nama_bulan']
            Bulan.save()
            return redirect('/siga/data/bulan', pk=Bulan.pk)
    else:
        form = Bulanform(instance=Bulan)
    return render(request, 'data/bulan/bulan_edit.html', {'form': form, 'bulan' : Bulan})


def deletebulan(request,pk):
    Bulan = bulan.objects.get(pk=pk)
    Bulan.delete()
    return redirect('/siga/data/bulan')

# ------------+
# BANK        |
# ------------+    
def viewbank(request):
    daftar_bank = bank.objects.all()
    pagination = Paginator(daftar_bank,5)

    page = request.GET.get('page','')
    bank_pg = pagination.get_page(page)
    return render(request, 'data/bank/bank.html', {'daftar_bank': bank_pg})

def addbank(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Bankform(form_data)
        if form.is_valid():
            Bank = bank(
                nama_bank=request.POST['nama_bank'],
                keterangan=request.POST['keterangan']
             )
            Bank.save()
            return redirect('/siga/data/bank')
    else:
        form = Bankform()
    return render(request, 'data/bank/bank_tambah.html', {'form': form})

def editbank(request,pk):
    Bank = bank.objects.get(pk=pk)
    if request.method == "POST":
        form = Bankform(request.POST, instance=Bank)
        if form.is_valid():
            Bank = form.save(commit=False)
            nama_bank = request.POST['nama_bank']
            Bank.save()
            return redirect('/siga/data/bank', pk=Bank.pk)
    else:
        form = Bankform(instance=Bank)
    return render(request, 'data/bank/bank_edit.html', {'form': form, 'bank' : Bank})


def deletebank(request,pk):
    Bank = bank.objects.get(pk=pk)
    Bank.delete()
    return redirect('/siga/data/bank')

# --------------+
# JABATAN       |
# --------------+  
def viewjabatan(request):
    daftar_jabatan = jabatan.objects.all()
    pagination = Paginator(daftar_jabatan,5)

    page = request.GET.get('page','')
    jabatan_pg = pagination.get_page(page)
    return render(request, 'data/jabatan/jabatan.html', {'daftar_jabatan': jabatan_pg})

def addjabatan(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Jabatanform(form_data)
        if form.is_valid():
            Jabatan = jabatan(
                nama_jabatan=request.POST['nama_jabatan'],
                keterangan=request.POST['keterangan']
             )
            Jabatan.save()
            return redirect('/siga/data/jabatan')
    else:
        form = Jabatanform()
    return render(request, 'data/jabatan/jabatan_tambah.html', {'form': form})

def editjabatan(request,pk):
    Jabatan = jabatan.objects.get(pk=pk)
    if request.method == "POST":
        form = Jabatanform(request.POST, instance=Jabatan)
        if form.is_valid():
            Jabatan = form.save(commit=False)
            nama_jabatan = request.POST['nama_jabatan']
            Jabatan.save()
            return redirect('/siga/data/jabatan', pk=Jabatan.pk)
    else:
        form = Jabatanform(instance=Jabatan)
    return render(request, 'data/jabatan/jabatan_edit.html', {'form': form, 'jabatan' : Jabatan})


def deletejabatan(request,pk):
    Jabatan = jabatan.objects.get(pk=pk)
    Jabatan.delete()
    return redirect('/siga/data/jabatan')



# ---------------+
# GAJI POKOK     |
# ---------------+  
def viewgajipokok(request):
    daftar_gajipokok = gajipokok.objects.all()
    pagination = Paginator(daftar_gajipokok,5)

    page = request.GET.get('page','')
    gajipokok_pg = pagination.get_page(page)
    return render(request, 'gaji/gajipokok/gajipokok.html', {'daftar_gajipokok': gajipokok_pg})

def addgajipokok(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Gajipokokform(form_data)
        if form.is_valid():
            Gajipokok = gajipokok(
                tgl_bayar=request.POST['tgl_bayar'],
                nama_pegawai=request.POST['nama_pegawai'],
                jabatan=request.POST['jabatan'],
                tmt=request.POST['tmt'],
                gaji_pokok=request.POST['gaji_pokok']
             )
            Gajipokok.save()
            return redirect('/siga/gaji/gajipokok')
    else:
        form = Gajipokokform()
    return render(request, 'gaji/gajipokok/gajipokok_tambah.html', {'form': form})

def editgajipokok(request,pk):
    Gajipokok = gajipokok.objects.get(pk=pk)
    if request.method == "POST":
        form = Gajipokokform(request.POST, instance=Gajipokok)
        if form.is_valid():
            Gajipokok = form.save(commit=False)
            tgl_bayar = request.POST['tgl_bayar']
            Gajipokok.save()
            return redirect('/siga/gaji/gajipokok', pk=Gajipokok.pk)
    else:
        form = Gajipokokform(instance=Gajipokok)
    return render(request, 'gaji/gajipokok/gajipokok_edit.html', {'form': form, 'gajipokok' : Gajipokok})


def deletegajipokok(request,pk):
    Gajipokok = gajipokok.objects.get(pk=pk)
    Gajipokok.delete()
    return redirect('/siga/gaji/gajipokok')



# -------------------+
# GAJI BULANAN       |
# -------------------+    
def viewbulanan(request):
    daftar_bulanan = bulanan.objects.all()
    pagination = Paginator(daftar_bulanan,5)

    page = request.GET.get('page','')
    bulanan_pg = pagination.get_page(page)
    return render(request, 'gaji/bulanan/bulanan.html', {'daftar_bulanan': bulanan_pg})

def addbulanan(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Bulananform(form_data)
        if form.is_valid():
            Bulanan = bulanan(
                nama_pegawai=request.POST['nama_pegawai'],
                jabatan=request.POST['jabatan'],
                gaji_pokok=request.POST['gaji_pokok'],
                bonus=request.POST['bonus'],
                bpjs=request.POST['bpjs'],
                pinjaman=request.POST['pinjaman'],
                total_gaji=request.POST['total_gaji'],
                periode_pembayaran=request.POST['periode_pembayaran']
             )
            Bulanan.save()
            return redirect('/siga/gaji/bulanan')
    else:
        form = Bulananform()
    return render(request, 'gaji/bulanan/bulanan_tambah.html', {'form': form})

def editbulanan(request,pk):
    Bulanan = bulanan.objects.get(pk=pk)
    if request.method == "POST":
        form = Bulananform(request.POST, instance=Bulanan)
        if form.is_valid():
            Bulanan = form.save(commit=False)
            nama_pegawai = request.POST['nama_pegawai']
            Bulanan.save()
            return redirect('/siga/gaji/bulanan', pk=Bulanan.pk)
    else:
        form = Bulananform(instance=Bulanan)
    return render(request, 'gaji/bulanan/bulanan_edit.html', {'form': form, 'bulanan' : Bulanan})


def deletebulanan(request,pk):
    Bulanan = bulanan.objects.get(pk=pk)
    Bulanan.delete()
    return redirect('/siga/gaji/bulanan')

def printbulanan(request,pk):
    Bulanan = bulanan.objects.get(pk=pk)
    Bulanan.print()
    return redirect('/siga/gaji/bulanan')

# ---------------+
# Pinjaman      |
# ---------------+   
def viewpinjaman(request):
    daftar_pinjaman = pinjaman.objects.all()
    pagination = Paginator(daftar_pinjaman,5)

    page = request.GET.get('page','')
    pinjaman_pg = pagination.get_page(page)
    return render(request, 'gaji/pinjaman/pinjaman.html', {'daftar_pinjaman': pinjaman_pg})

def addpinjaman(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Pinjamanform(form_data)
        if form.is_valid():
            Pinjaman = pinjaman(
                nama_pegawai=request.POST['nama_pegawai'],
                tanggal_pinjam=request.POST['tanggal_pinjam'],
                jumlah_pinjam=request.POST['jumlah_pinjam'],
                jenis_transaksi=request.POST['jenis_transaksi'],
                keterangan=request.POST['keterangan'] 
             )
            Pinjaman.save()
            return redirect('/siga/gaji/pinjaman')
    else:
        form = Pinjamanform()
    return render(request, 'gaji/pinjaman/pinjaman_tambah.html', {'form': form})    

def editpinjaman(request,pk):
    Pinjaman = pinjaman.objects.get(pk=pk)
    if request.method == "POST":
        form = Pinjamanform(request.POST, instance=Pinjaman)
        if form.is_valid():
            Pinjaman = form.save(commit=False)
            nama_pegawai=request.POST['nama_pegawai']
            tanggal_pinjam=request.POST['tanggal_pinjam']
            jumlah_pinjam=request.POST['jumlah_pinjam']
            jenis_transaksi=request.POST['jenis_transaksi']
            keterangan=request.POST['keterangan'] 
            Pinjaman.save()
            return redirect('/siga/data/pinjaman', pk=Pinjaman.pk)
    else:
        form = Pinjamanform(instance=Pinjaman)
    return render(request, 'gaji/pinjaman/pinjaman_edit.html', {'form': form, 'pinjaman' : Pinjaman})

def deletepinjaman(request,pk):
    Pinjaman = pinjaman.objects.get(pk=pk)
    Pinjaman.delete()
    return redirect('/siga/gaji/pinjaman')

# -------------+
# Pegawai      |
# -------------+  


def viewpegawai(request):
    daftar_pegawai = pegawai.objects.all()
    pagination = Paginator(daftar_pegawai,5)

    page = request.GET.get('page','')
    pegawai_pg = pagination.get_page(page)
    return render(request, 'pegawai/pegawai_aktif/pegawai.html', {'daftar_pegawai': pegawai_pg})

def addpegawai(request):
    if request.method == 'POST':
        form_data = request.POST
        form = Pegawaiform(form_data)
        if form.is_valid():
            Pegawai = pegawai(
                no_pegawai=request.POST['no_pegawai'],
                nama_pegawai=request.POST['nama_pegawai'],
                no_ktp=request.POST['no_ktp'],
                tempat_lahir=request.POST['tempat_lahir'],
                tgl_lahir=request.POST['tgl_lahir'],
                alamat=request.POST['alamat'],
                npwp=request.POST['npwp'],
                email=request.POST['email'],
                no_hp=request.POST['no_hp'],
                foto=request.POST['foto'],
                bank=request.POST['bank'],
                nama=request.POST['nama'],
                no_rekening=request.POST['no_rekening'],
                kantor_cabang=request.POST['kantor_cabang'],
                no_surat_keterangan=request.POST['no_surat_keterangan'],
                jabatan=request.POST['jabatan'],
                jenis_perjanjian=request.POST['jenis_perjanjian'],
                mulai_perjanjian_kerja=request.POST['mulai_perjanjian_kerja'],
                akhir_perjanjian_kerja=request.POST['akhir_perjanjian_kerja'],
                upah=request.POST['upah'],
                keterangan=request.POST['keterangan']
             )
            Pegawai.save()
            return redirect('/siga/pegawai/pegawai_aktif')
    else:
        form = Pegawaiform()
    return render(request, 'pegawai/pegawai_aktif/pegawai_tambah.html', {'form': form})

def editpegawai(request,pk):
    Pegawai = pegawai.objects.get(pk=pk)
    if request.method == "POST":
        form = Pegawaiform(request.POST, instance=Pegawai)
        if form.is_valid():
            Pegawai = form.save(commit=False)
            no_pegawai = request.POST['no_pegawai']
            Pegawai.save()
            return redirect('/siga/pegawai/pegawai_aktif', pk=Pegawai.pk)
    else:
        form = Pegawaiform(instance=Pegawai)
    return render(request, 'pegawai/pegawai_aktif/pegawai_edit.html', {'form': form, 'pegawai' : Pegawai})


def deletepegawai(request,pk):
    Pegawai = pegawai.objects.get(pk=pk)
    Pegawai.delete()
    return redirect('/siga/pegawai/pegawai_aktif')

def showpegawai(request,pk):
    Pegawai = pegawai.objects.get(pk=pk)
    return render(request, 'pegawai/pegawai_aktif/pegawai_show.html', {'pegawai' : Pegawai})