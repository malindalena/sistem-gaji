# Generated by Django 2.2.2 on 2019-08-08 02:22

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('sigas', '0010_pinjaman'),
    ]

    operations = [
        migrations.CreateModel(
            name='pegawai',
            fields=[
                ('id_pegawai', models.AutoField(primary_key=True, serialize=False)),
                ('no_pegawai', models.CharField(max_length=100)),
                ('nama_pegawai', models.CharField(max_length=100)),
                ('no_ktp', models.CharField(max_length=100)),
                ('tempat_lahir', models.CharField(max_length=100)),
                ('tgl_lahir', models.DateField()),
                ('alamat', models.CharField(max_length=100)),
                ('npwp', models.CharField(max_length=100)),
                ('email', models.EmailField(blank=True, max_length=70, null=True, unique=True)),
                ('no_hp', models.CharField(max_length=13)),
                ('foto', models.FileField(upload_to='documents/')),
                ('bank', models.CharField(max_length=50)),
                ('nama', models.CharField(max_length=100)),
                ('no_rekening', models.DecimalField(decimal_places=0, max_digits=50)),
                ('kantor_cabang', models.CharField(max_length=100)),
                ('no_surat_keterangan', models.CharField(max_length=100)),
                ('jabatan', models.CharField(max_length=100)),
                ('jenis_perjanjian', models.CharField(max_length=100)),
                ('mulai_perjanjian_kerja', models.DateField()),
                ('akhir_perjanjian_kerja', models.DateField()),
                ('upah', models.DecimalField(decimal_places=3, max_digits=20)),
                ('keterangan', models.CharField(max_length=100)),
            ],
            options={
                'db_table': 'tb_pegawai',
            },
        ),
    ]
